#!/bin/bash
code Documents/Koulu/24\ Kevat/Ohjelmistojen_yllapito_ja_testaus/projects/stam/unit-test

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

# git remote add origin https://gitlab.com/{username}/unit-test
# git remote set-url origin ...
# git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

# test files that goes to staging area
git add .  --dry-run