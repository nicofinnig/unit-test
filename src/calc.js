// TODO: arithmetic operations
/**
 * Adds two numbers together.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b; 

const subtract = (minuend, subrahend) => {
  return minuend - subrahend;
};
const multiply = (multiplier, multiplicant) => {
  return multiplier * multiplicant;
}

/**
 * Divides two numbers
 * @param {number} divident 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error} 0 division 
 */
const divide = (divident, divisor) => {
  if (divisor == 0) throw new Error("0 division not allowed");
    const fraction = divident / divisor;
    return fraction;
}
export default { add, subtract, multiply, divide }