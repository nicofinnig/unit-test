import { assert, expect, should } from "chai";
import { describe } from "mocha";
import calc from "../src/calc.js";

describe("Calc.js", () => {
  let myVar;
  before(() => {    // Ennen testejä
    myVar = 0;
  });
  beforeEach(() => myVar++) // Ennen jokaista testiä
  after(() => console.log(`myVar: ${myVar}`));
  it("can add numbers", () => {
    expect(calc.add(2,2)).to.equal(4);
    expect(calc.add(2,3)).to.equal(5);
  });
  it("can subtract numbers", () => {
    assert(calc.subtract(5,1) === 4); 
    assert(calc.subtract(3,2) === 1); 
    //expect(calc.subtract(5,1)).to.equal(4);
  });
  it("can multiply numbers", () => {
    should().exist(calc.multiply)
    expect(calc.multiply(3,3)).to.equal(9);
  });
  it("can divide numbers", () => {
    expect(calc.divide(2,4)).to.equal(0.5);
    expect(calc.divide(6,2)).to.equal(3);
  });
  it("0 division throws and error", () => {
    const err_msg = "0 division not allowed";
    expect(() => calc.divide(1,0))    // HUOM! Nuolifunktio tähän, että toimii! Koska parametreja!
      .to
      .throw(err_msg);
  })
});